package cn.turing.firecontrol.device.weixin;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import weixin.popular.api.MessageAPI;
import weixin.popular.api.TokenAPI;
import weixin.popular.bean.BaseResult;
import weixin.popular.bean.token.Token;

/**
 * 消息推送
 * @author 高元泰
 */
@RestController
@RequestMapping("weixin")
public class Popular {

    public class WeiXinController {
    /*private final String appID = "wxfb95760bbe71b977";
    private final String appSecret = "5fc9b5937cd9dd01d7b4bf8044cd0820";*/

        /**
         * 发送消息给微信客户端，
         *
         * @return
         */
        @RequestMapping("sendMessage")
        public BaseResult sendMessage(String appId, String appSecret) {

            Token token = TokenAPI.token(appId, appSecret);
            BaseResult baseResult = MessageAPI.messageCustomSend(token.getAccess_token(), "你好");
            return baseResult;
        }
    }
}
