/*
 * Copyright (c) 2021.  北京维数科技有限公司
 * Date :  2021/3/25
 * Author ：F12 Console=> atob("MzMxNDkxODAwQHFxLmNvbQ==")
 * Version ：1.0.0
 */

import request from '@/utils/request'

//结果列表的接口
export function page(query) {
  return request({
    url:'/api/device/deviceSensor/pageOutdoorList',
    method: 'get',
    params: query
  })
}
//右侧树的接口
export function fetchTree(query) {
  return request({
    url: '/api/device/deviceHardwareFacilities/select',
    method: 'get',
    params: query
  })
}
//高级查询条件接口
export function advanceSelected(query) {
  return request({
    url:'/api/device/deviceSensor/getOutdoorSelected',
    method:'get',
    params: query
  })
}
//默认选中的接口
export function firstBuild(query) {
  return request({
    url: '/api/device/deviceHardwareFacilities/selectFirst',
    method: 'get',
    params: query
  })
}

//厂商系列型号和所属系统
export function getAllInfoObj(query) {
  return request({
    url: '/api/device/deviceSensorType/selectTypeOutdoor',
    method: 'get',
    params: query
  })
}

//添加传感器
export function addObj(obj) {
  return request({
    url: '/api/device/deviceSensor/addOutdoor',
    method: 'post',
    data: obj
  })
}

//所属设施
export function relateOutdoor(query) {
  return request({
    url: '/api/device/deviceHardwareFacilities/selectByNameAndType',
    method: 'get',
    params: query
  })
}

//编辑提交
export function updateObj(obj) {
  return request({
    url: '/api/device/deviceSensor/updateOutdoor',
    method: 'post',
    data: obj
  })
}

//通用删除接口
export function delObj(id) {
  return request({
    url: '/api/device/deviceSensor/delete?id=' + id,
    method: 'get'
  })
}
