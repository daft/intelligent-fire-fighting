/*
 * Copyright (c) 2021.  北京维数科技有限公司
 * Date :  2021/3/25
 * Author ：F12 Console=> atob("MzMxNDkxODAwQHFxLmNvbQ==")
 * Version ：1.0.0
 */

import request from '@/utils/request'

export function page(query) {
return request({
url: '/api/device/deviceSensorType/pageList',
method: 'get',
params: query
})
}


export function pageSelected() {
    return request({
    url: '/api/device/deviceSensorType/getSelected',
    method: 'get',
    })
}

export function addObj(obj) {
    return request({
    url: '/api/device/deviceSensorType/add',
    method: 'post',
    data: obj
    })
}

export function getObj(id) {
return request({
url: '/api/device/deviceSensorType/' + id,
method: 'get'
})
}

export function delObj(id) {
    return request({
    url: '/api/device/deviceSensorType/delete?id=' + id,
    method: 'get'
    })
}


export function deleteQuery(id) {
    return request({
    url: '/api/device/deviceSensorType/deleteQuery?id='+id,
    method: 'get'
    })
}

export function putObj(obj) {
    return request({
    url: '/api/device/deviceSensorType/update',
    method: 'post',
    data: obj
    })
}
