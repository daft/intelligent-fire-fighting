/*
 * Copyright (c) 2021.  北京维数科技有限公司
 * Date :  2019/4/8
 * Author ：F12 Console=> atob("MzMxNDkxODAwQHFxLmNvbQ==")
 * Version ：1.0.0
 */

import request from '@/utils/request'

export function page(query) {
  return request({
    url: '/api/datahandler/noticeLog/listNoticeLog',
    method: 'get',
    params: query
  })
}
