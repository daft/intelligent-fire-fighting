/*
 * Copyright (c) 2021.  北京维数科技有限公司
 * Date :  2021/3/25
 * Author ：F12 Console=> atob("MzMxNDkxODAwQHFxLmNvbQ==")
 * Version ：1.0.0
 */

import request from '@/utils/request'

export function page(query) {
  return request({
    url: '/api/admin/user/list',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/api/admin/user/add',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/api/admin/user/' + id,
    method: 'get'
  })
}

export function getUserRole(query){
  return request({
    url: '/api/admin/group/listByUser',
    method: 'get',
    params: query
  })
}

export function delObj(query) {
  return request({
    url: '/api/admin/user/delete',
    method: 'get',
    params: query
  })
}

export function updateObj(obj) {
  return request({
    url: '/api/admin/user/update',
    method: 'post',
    data: obj
  })
}


export function changePassword(data) {
  return request({
    url: '/api/admin/user/changePassword',
    method: 'post',
    params: data
  })
}

export function getGroup(){
  return request({
    url:'/api/admin/group/all',
    method:'get'
  })
}
