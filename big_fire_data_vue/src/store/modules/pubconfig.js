/*
 * Copyright (c) 2021.  北京维数科技有限公司
 * Date :  2021/3/25
 * Author ：F12 Console=> atob("MzMxNDkxODAwQHFxLmNvbQ==")
 * Version ：1.0.0
 */

import {
  getImgAuth
} from '@/api/device/tenetConfig';
const pubconfig={
	state:{
		planSetup:0
	},
	mutations:{
		SET_IMG_AUTH:(state,planSetup)=>{
			state.planSetup=planSetup;
		}
	},
	actions:{
		getImgAuth({ commit }){
			return new Promise((resolve, reject) => {
				getImgAuth().then((res)=>{
					if (res.status==200) {
						commit('SET_IMG_AUTH',res.data);
						resolve();
					}
				})
			})
		}
	}
}
export default pubconfig
