package cn.turing.firecontrol.device.alarm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;


@EnableScheduling
@SpringBootApplication
public class DeviceAlarmApp {

    public static void main(String[] args) {
        SpringApplication.run(DeviceAlarmApp.class, args);
    }
}
