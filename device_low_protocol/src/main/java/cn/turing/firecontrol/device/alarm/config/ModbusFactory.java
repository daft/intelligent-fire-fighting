package cn.turing.firecontrol.device.alarm.config;

import com.serotonin.io.serial.SerialParameters;
import com.serotonin.modbus4j.ModbusMaster;
import com.serotonin.modbus4j.msg.ReadHoldingRegistersRequest;
import com.serotonin.modbus4j.msg.ReadHoldingRegistersResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;


/**
 * modbusFactory 配置
 */
@Slf4j
@Component
public class ModbusFactory {

    public static ModbusMaster master;
    // 设定MODBUS网络上从站地址
    public final static int SLAVE_ADDRESS = 1;
    //串行波特率
    public final static int BAUD_RATE = 9600;

    /**
     * 使用注解： @PostConstruct
     * 效果：在Bean初始化之后（构造方法和@Autowired之后）执行指定操作。经常用在将构造方法中的动作延迟。
     * 备注：Bean初始化时候的执行顺序： 构造方法 -> @Autowired -> @PostConstruct
     * @throws Exception
     */
    @PostConstruct
    public void init() throws Exception{
        try {
            /* 创建ModbusFactory工厂实例 */
            com.serotonin.modbus4j.ModbusFactory modbusFactory = new com.serotonin.modbus4j.ModbusFactory();
            /* 创建ModbusMaster实例 */
            SerialParameters serialParameters = new SerialParameters();
            // 设定MODBUS通讯的串行口
            serialParameters.setCommPortId("COM3");
            // 设定成无奇偶校验
            serialParameters.setParity(0);
            // 设定成数据位是8位
            serialParameters.setDataBits(8);
            // 设定为1个停止位
            serialParameters.setStopBits(1);
            // 设定端口名称
            serialParameters.setPortOwnerName("烟感");
            // 设定端口波特率
            serialParameters.setBaudRate(BAUD_RATE);
            master = modbusFactory.createRtuMaster(serialParameters);
            master.init();
        } catch (Exception e) {
            e.printStackTrace();
            log.error("设备异常");
        }

    }

    public static void destroy() throws Exception {
        master.destroy();
    }

    /**
     * 批量读保持寄存器上的内容
     *
     * @param slaveId 从站地址
     * @param start   起始地址的偏移量
     *      * @param len 待读寄存器的个数
     */
    public short[] batchReadHoldingRegisters(int slaveId, int start, int len) throws Exception {
        ReadHoldingRegistersRequest request = new ReadHoldingRegistersRequest(slaveId, start, len);
        ReadHoldingRegistersResponse response = (ReadHoldingRegistersResponse) master.send(request);
        return response.getShortData();

    }
}
