



## TMC智慧消防云平台部署说明

### 数据库环境准备
- mysql 5.7
- redis 5.0.0
- elasticsearch 6.5.4
- rabbitmq 3.6.9
使用根目录下maven 配置文件
mysql数据库资源创建好后，执行如下sql创建初始化数据库
```
CREATE DATABASE `turing-admin` CHARACTER SET 'utf8' COLLATE 'utf8_general_ci';
CREATE DATABASE `turing-auth` CHARACTER SET 'utf8' COLLATE 'utf8_general_ci';
CREATE DATABASE `turing-datahandler` CHARACTER SET 'utf8' COLLATE 'utf8_general_ci';
CREATE DATABASE `turing-device` CHARACTER SET 'utf8' COLLATE 'utf8_general_ci';
```
并导入sql目录下的各个sql文件来创建初始化表和初始化数据

### 基础配置准备
SpringCloud使用gitee来作为配置中心
[示例配置文件地址](https://gitee.com/zkturing-tmc/fire_control_config_example)  

修改gitee上各个微服务配置下的数据库链接信息，并对应修改微服务代码配置yml里面的gitee配置信息

### 代码编译打包
1. 编译各个微服务代码，生成对应jar包
2. 执行各个服务文件夹下Dockerfile文件，打包成docker镜像


### 启动服务
对应修改docker-compose.yml配置文件，执行`docker compose up -d` 即可启动服务

本地启动顺序为：CenterBootstrap -> ConfigServerBootstrap -> AuthBootstrap ->  AdminBootstrap -> GateBootstrap  。

DeviceBootstrap、DataHandlerBootstrap 、DataCollectionBootstrap 可在前五个服务启动完成后无序启动。

### 前端代码链接
[https://gitee.com/zkturing-tmc/big_fire_data_vue](https://gitee.com/zkturing-tmc/big_fire_data_vue)

### Nginx配置示例
```
user www-data;
worker_processes auto;

worker_rlimit_nofile 51200;

events {

  use epoll;
  worker_connections 51200;
  multi_accept on;
}
http {
    include       mime.types;
    default_type  application/octet-stream;

    server_names_hash_bucket_size   128;

    client_header_buffer_size   32k;

    large_client_header_buffers 4 32k;
    log_format access '$remote_addr - $remote_user [$time_local] "$request" '
            '$status $body_bytes_sent "$http_referer" '
            '"$http_user_agent" "$http_x_forwarded_for"';
    access_log  /var/log/nginx/access.log   access;
    sendfile    on;
    tcp_nopush  on;
    tcp_nodelay on;
    keepalive_timeout   65;
    server_tokens   off;
    client_body_buffer_size 1024k;
   
    proxy_connect_timeout   150s;
    proxy_send_timeout      150s;
    proxy_read_timeout      150s;

    proxy_buffer_size       512k;
    proxy_buffers           64 64k;
    proxy_busy_buffers_size 2048k;
    proxy_temp_file_write_size 2048k;


    gzip    on;
    gzip_min_length 1k;
    gzip_buffers 4 16k;
    gzip_http_version 1.1;
    gzip_comp_level 2;
    gzip_types text/plain application/x-javascript text/css application/xml;
    gzip_vary   on;    

    client_header_timeout 120s;     
    client_body_timeout 120s;  

    client_max_body_size 300M; 

    upstream mycluster{
        # 对应后端服务地址
        server turing-gate.tmc-v1:8765 weight=1;
    }

    upstream websocket{
        # 对应后端服务地址
        server turing-datahandler.tmc-v1:2345  weight=1;
    }

    server {
        listen 80;
        server_name _;
        #charset koi8-r;
        #access_log  /var/log/nginx/host.access.log  main;

        location / {
            root   /usr/share/nginx/html;
            index  index.html index.htm;
        }

        location /threed/ {
            root  /usr/share/nginx/threed;
        }

        location /api/ {
            #proxy_next_upstream http_502 http_504 error timeout invalid_header;
            proxy_set_header Connection "Keep-Alive";
            proxy_pass http://mycluster;
            proxy_set_header   X-Real-IP        $remote_addr:8765;
            proxy_set_header   Host             $host;
            proxy_set_header   X-Forwarded-For  $proxy_add_x_forwarded_for;
            proxy_set_header   X-Forwarded-Proto $scheme;
        }

        location /alarm/websocket {
            proxy_pass http://websocket;
            proxy_redirect off;
            proxy_http_version 1.1;
            proxy_set_header Upgrade $http_upgrade;
            proxy_set_header Connection "upgrade";
            proxy_set_header Host $host:$server_port;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        }

    }

}
```

### 测试账号

账号：admin

密码：Turing2018

### 常见问题

#### 一、jar包缺失

可以使用项目中的settings文件作为maven配置，settings文件中私服有项目所需的所有包。也可以在pom文件中配置本地包，目前缺失jar已经添加对应模块的lib文件加下。pom文件：

```xml
<!--使用依赖本地jar包-->
<dependency>
    <groupId>com.huawei</groupId>
    <artifactId>vms-util</artifactId>
    <version>1.0</version>
    <scope>system</scope>
    <systemPath>${basedir}/src/main/resources/lib/vms-util-1.0.jar</systemPath>
</dependency>
<!--使用私服-->
<!--<dependency>
    <groupId>com.huawei</groupId>
    <artifactId>vms-util</artifactId>
    <version>1.0</version>
</dependency>-->
```



####  二、auth.serviceId 找不到

如果项目启动报错 auth.serviceId 找不到 请确认配置中心服务是否启动、项目获取配置中心auth-demo是否正确。



#### 三、elasticsearch 本地部署问题

elasticsearch.yml 文件要和 application-demo.yml 中的配置一致。如：

application-demo.yml 文件

```
elasticsearch:
    cluster-name: fire-control
    cluster-nodes: 127.0.0.1:9300
```
elasticsearch.yml 文件
```java
cluster.name: fire-control
network.host: 127.0.0.1
```

**注意：cluster-nodes 不要加http**

#### 四、配置文件问题

**注意：请仔细修改配置文件，将eureka、redis、rabbitMq、elasticsearch、数据库等配置修改为自己的配置，各个服务功能模块下的bootstrap.yml文件配置正确，例如：**

###### turing-center 服务下的 bootstrap.yml

```
spring:
    application:
        name: turing-center

server:
    port: 8761 #启动端口

eureka:
    client:
        registerWithEureka: false  #false:不作为一个客户端注册到注册中心
        fetchRegistry: false      #为true时，可以启动，但报异常：Cannot execute request on any known server
        defaultZone: http://127.0.0.1:8761/eureka/ # 填写本地或者服务器 eureka 服务的ip地址
```

###### turing-auth 服务下的 bootstrap.yml

```
spring:
  profiles:
    active: demo # 和git上配置文件名称一致
  cloud:
    config:
      discovery:
        service-id: turing-config
        enabled: true
      label: master # git 分支  # 和git上配置分支名一致
      name: auth
      profile: ${spring.profiles.active}

management:
  security:
    enabled: false

---
spring:
  profiles: demo  # 和git上配置文件名称一致
eureka:
  instance:
    statusPageUrlPath: /swagger-ui.html
    healthCheckUrlPath: /health
    # docker 部署开启,记得将IP修改为部署所在服务器的外网IP
    leaseRenewalIntervalInSeconds: 5
  client:
    serviceUrl:
      defaultZone: http://host/eureka/  ## 注意 defaultZone 要与 turing-center 服务下的 bootstrap.yml中的 defaultZone 一致。
```

**如果还有疑惑的请百度查询：springcloud eureka 和 config  如何配置的详细信息和步骤，在此不多做说明**。



##### 1、AuthBootstrap 启动报错

```
java.lang.IllegalStateException: Service id not legal hostname (${jwt.user-service})
```

此报错为认证中心（AuthBootstrap）没有拿到 application-demo.yml 配置文件，请确认服务的注册中心 CenterBootstrap 和  服务配置文件服务 ConfigServerBootstrap  正常启动了（配置不清楚的请百度查询 springcloud eureka 和 config  如何配置的详细信息和步骤）；确保 bootstrap.yml 配置问题中的 defaultZone 要与 turing-center 服务下的 bootstrap.yml中的 defaultZone 一致。

##### 2、AdminBootstrap 启动报错

```
com.sun.jersey.api.client.ClientHandlerException: java.net.UnknownHostException: host

```

此报错为配置文件配置错误，请确认服务的注册中心 CenterBootstrap 和  服务配置文件服务 ConfigServerBootstrap  正常启动了（配置不清楚的请百度查询 springcloud eureka 和 config  如何配置的详细信息和步骤）；将 admin-demo.yml 中数据库配置修改正确，**不要使用示例配置地址 host 除非配置所在数据库服务器的hosts文件，指定host为此服务器的映射。**

```
spring:
  application:
    name: turing-admin
  datasource:                   
    url: jdbc:mysql://host:3306/turing-admin?useUnicode=true&characterEncoding=UTF8 ## 请配置正确的数据库地址，不要使用 host 除非配置所在数据库服务器的hosts文件，指定host为此服务器的映射。
```

##### 3、GateBootstrap 启动报错

```
org.springframework.data.redis.RedisConnectionFailureException: Cannot get Jedis connection; nested exception is redis.clients.jedis.exceptions.JedisConnectionException: Could not get a resource from the pool
```

此报错为配置文件中redis地址配置错误。请确认服务的注册中心 CenterBootstrap 和  服务配置文件服务 ConfigServerBootstrap  正常启动了（配置不清楚的请百度查询 springcloud eureka 和 config  如何配置的详细信息和步骤）；将 application-demo.yml 中redis配置修改正确，**不要使用示例配置地址 host 除非配置所在redis服务器的hosts文件，指定host为此服务器的映射。**

```
  redis:
    host: host     # 使用正确的redis所在服务器地址
    password: Turing2018  # 使用正确的redis所设置的密码
    port: 6379
    database: 0
```

##### 4、DataCollectionBootstrap 启动报错

```
UnknownHostException: turing-center
com.sun.jersey.api.client.ClientHandlerException: java.net.UnknownHostException: turing-center

```

此报错为配置文件配错，请确认服务的注册中心 CenterBootstrap 和  服务配置文件服务 ConfigServerBootstrap  正常启动了（配置不清楚的请百度查询 springcloud eureka 和 config  如何配置的详细信息和步骤）；请将DataCollectionBootstrap服务中的bootstrap.yml的服务注册中心地址配置正确。

```
eureka:
  instance:
    #    statusPageUrlPath: /swagger-ui.html
    healthCheckUrlPath: /health
    prefer-ip-address: true
    # ip-address:  ${spring.cloud.client.ipAddress}
    ip-address:  127.0.0.1    # 填写本地或者服务器ip地址
    leaseRenewalIntervalInSeconds: 5
  client:
    serviceUrl:
      #defaultZone: http://${CENTER_HOST:turing-center}:${CENTER_PORT:8761}/eureka/
      defaultZone: http://127.0.0.1:8761/eureka/ # 填写本地或者服务器 eureka 服务的ip地址
```

#### 五、Command line is too long

idea问题，请参考 https://blog.csdn.net/feitianlongfei/article/details/80976953