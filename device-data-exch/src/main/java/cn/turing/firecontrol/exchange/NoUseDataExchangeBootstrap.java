package cn.turing.firecontrol.exchange;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NoUseDataExchangeBootstrap {
    public static void main(String[] args){
        SpringApplication.run(NoUseDataExchangeBootstrap.class,args);
    }
}