package cn.turing.firecontrol.exchange.service;

import java.util.ArrayList;

/**
 * @author Richardwoo
 * @time 2021-05-15
 *
 * 类说明
 */

public interface ElasticService {

	String getIndexDataCount(String indexName);
	
	String createIndex(String indexName);

	String deleteIndex(String indexName);

	ArrayList<String> getAllIndex();

	Boolean bulkData2Es(String indexName);

	ArrayList<String> queryData(String queryParm);
	
//	queryDataFrom

}
