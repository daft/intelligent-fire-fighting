package cn.turing.firecontrol.common.util;

import org.springframework.web.bind.annotation.RequestMapping;
import weixin.popular.api.MessageAPI;
import weixin.popular.api.TokenAPI;
import weixin.popular.api.UserAPI;
import weixin.popular.bean.BaseResult;
import weixin.popular.bean.message.message.TextMessage;
import weixin.popular.bean.token.Token;
import weixin.popular.bean.user.FollowResult;

import java.util.Arrays;

/**
 *
 * @author 高元泰
 */
public class WeiXinUtil {
    /**
     * 发送消息给微信客户端，
     *
     * @return
     */
    private final static String appId = "wx470d56633e9a0b34";
    private final  static String appSecret = "1713015cf20dedbabb0bdb92c1002cce";
    public static void sendMessage(String content) {
        Token token = TokenAPI.token(appId, appSecret);
        FollowResult followResult = UserAPI.userGet(token.getAccess_token(), null);
        String[] openid = followResult.getData().getOpenid();
        Arrays.asList(openid).forEach(touser->{
            TextMessage message = new TextMessage(touser,content);
            BaseResult baseResult = MessageAPI.messageCustomSend(token.getAccess_token(), message);
        });
    }
}
