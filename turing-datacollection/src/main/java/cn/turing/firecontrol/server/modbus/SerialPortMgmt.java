package cn.turing.firecontrol.server.modbus;

import com.serotonin.io.serial.SerialParameters;
import com.serotonin.modbus4j.ModbusMaster;
import gnu.io.SerialPort;
import lombok.Data;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

@Data
public class SerialPortMgmt {
    private ModbusMaster master;   //主站
    private int slave_address = 1; // 设定MODBUS网络上从站地址
    private SerialParameters serialParameters=new SerialParameters(); //设置串口参数
    private SerialInputStream serialInputStream;
    private SerialOutputStream serialOutputStream;
    private BlockingQueue<String> blockingMsgQueue = new LinkedBlockingQueue<String>();   //堵塞队列用来存放读到的数据 线程安全
}