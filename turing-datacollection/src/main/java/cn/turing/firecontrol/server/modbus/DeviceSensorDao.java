package cn.turing.firecontrol.server.modbus;

import cn.turing.firecontrol.server.modbus.AuditAction;
import cn.turing.firecontrol.server.modbus.Action;
import cn.turing.firecontrol.server.modbus.DeviceSensor;
import org.springframework.stereotype.Service;

public interface DeviceSensorDao {
    DeviceSensor selectByPrimaryKey(Long id);


    @AuditAction(action = Action.ADD, targetTable = "device_sensor")
    int insert(DeviceSensor record);
    @AuditAction(action = Action.ADD, targetTable = "device_sensor")
    int insertSelective(DeviceSensor record);

    @AuditAction(action = Action.UPDATE, targetTable = "device_sensor")
    int updateByPrimaryKeySelective(DeviceSensor record);
    @AuditAction(action = Action.UPDATE, targetTable = "device_sensor")
    int updateByPrimaryKey(DeviceSensor record);

    @AuditAction(action = Action.DELETE, targetTable = "device_sensor")
    int deleteByPrimaryKey(Long id);
}
