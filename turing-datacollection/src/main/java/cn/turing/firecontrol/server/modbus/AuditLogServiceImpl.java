package cn.turing.firecontrol.server.modbus;

import cn.turing.firecontrol.server.modbus.AuditLogDao;
import cn.turing.firecontrol.server.modbus.AuditLog;
import cn.turing.firecontrol.server.modbus.AuditLogService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service("auditLogServiceImpl")
public class AuditLogServiceImpl implements AuditLogService {
    @Autowired
    AuditLogDao auditLogDao;

    @Override
    public AuditLog selectByPrimaryKey(Integer logNum) {
        return auditLogDao.selectByPrimaryKey(logNum);
    }

    @Transactional
    @Override
    public int insert(AuditLog record) {
        return auditLogDao.insert(record);
    }
    @Transactional
    @Override
    public int insertSelective(AuditLog record) {
        return auditLogDao.insertSelective(record);
    }
    @Transactional
    @Override
    public int updateByPrimaryKeySelective(AuditLog record) {
        return auditLogDao.updateByPrimaryKeySelective(record);
    }
    @Transactional
    @Override
    public int updateByPrimaryKey(AuditLog record) {
        return auditLogDao.updateByPrimaryKey(record);
    }

    @Transactional
    @Override
    public int deleteByPrimaryKey(Integer logNum) {
        return auditLogDao.deleteByPrimaryKey(logNum);
    }
}
