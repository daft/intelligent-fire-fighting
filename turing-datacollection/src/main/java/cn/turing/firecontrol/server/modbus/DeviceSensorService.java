package cn.turing.firecontrol.server.modbus;
import cn.turing.firecontrol.server.modbus.DeviceSensor;

public interface DeviceSensorService {
    int deleteByPrimaryKey(Long id);

    int insert(DeviceSensor record);

    int insertSelective(DeviceSensor record);

    DeviceSensor selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(DeviceSensor record);

    int updateByPrimaryKey(DeviceSensor record);
}
