package cn.turing.firecontrol.server.modbus;

import cn.turing.firecontrol.server.modbus.DeviceSensorDao;
import cn.turing.firecontrol.server.modbus.DeviceSensor;
import cn.turing.firecontrol.server.modbus.DeviceSensorService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service("deviceSensorService")
public class DeviceSensorServiceImpl implements DeviceSensorService {
    @Lazy
    @Autowired
    DeviceSensorDao deviceSensorDao;
    @Override
    public DeviceSensor selectByPrimaryKey(Long id) {
        return deviceSensorDao.selectByPrimaryKey(id);
    }
    @Transactional
    @Override
    public int deleteByPrimaryKey(Long id) {
        return deviceSensorDao.deleteByPrimaryKey(id);
    }
    @Transactional
    @Override
    public int insert(DeviceSensor record) {
        return deviceSensorDao.insert(record);
    }
    @Transactional
    @Override
    public int insertSelective(DeviceSensor record) {
        return deviceSensorDao.insertSelective(record);
    }
    @Transactional
    @Override
    public int updateByPrimaryKeySelective(DeviceSensor record) {
        return deviceSensorDao.updateByPrimaryKeySelective(record);
    }
    @Transactional
    @Override
    public int updateByPrimaryKey(DeviceSensor record) {
        return deviceSensorDao.updateByPrimaryKey(record);
    }
}
